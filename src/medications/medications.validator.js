const Joi = require('joi');
const mongoose = require('mongoose');

const { models } = require('../utils/constants');
const { formatValidationErrorMessages } = require('../utils/helpers');

const Medicines = mongoose.model(models.Medicines);

module.exports = {

  async validateSave(req, res, next) {
    const schema = Joi.object({
      name: Joi.string().regex(/^[-_a-zA-Z0-9]*$/)
      .required().messages({
        "string.pattern.base": "allowed only capital letters, numbers, '_'",
      }),
      code: Joi.string().regex(/^[_a-zA-Z0-9]*$/).required().messages({
        "string.pattern.base": "allowed only capital letters, numbers, '_'",
      }),
      weight: Joi.number().greater(0).max(500).required()
    });

    const { error } = schema.validate(req.body, { abortEarly: false });

    if (error) {
      const errorMessages = formatValidationErrorMessages(error);
      return res.status(422).json({
        status: 422,
        message: "failed validation",
        errorMessages
      });
    }

    const MedicineDuplicate = await Medicines.findOne({
      $or: [{ code: req.body.code }, { name: req.body.name }]
    });

    if (MedicineDuplicate && MedicineDuplicate._id) {
      return res.status(400).json({
        status: 400,
        message: `Medicine with code: "${MedicineDuplicate.code}" or name: "${MedicineDuplicate.name}" already exists`,
      });
    }
    next();
  },
};
