const express = require('express');
const { createMedicine, getAllMedicine } = require('./medications.controller');
const { validateSave } = require('./medications.validator');

const router = express.Router();

router.get('/', getAllMedicine);
router.post('/', validateSave, createMedicine);

module.exports = router;