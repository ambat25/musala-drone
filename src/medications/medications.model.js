const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const { models } = require('../utils/constants');

const MedicinesSchema = mongoose.Schema(
	{
		name: {
      maxLength: 100,
			required: true,
			type: String,
			unique: true,
      match: [(/[A-Za-z0-9_-]$/), `allowed only letters, numbers, '-', '_'`]

		},
		code: {
      maxLength: 100,
			required: true,
			type: String,
			unique: true,
      match: [(/[A-Z0-9_]$/), `allowed only capital letters, numbers, '_'`]
		},
		weight: {
			required: true,
			type: Number,
      max: 500,
		},
		image: {
			required: false,
			type: String,
		}
	},
	{
		timestamps: true,
	}
);

MedicinesSchema.plugin(mongoosePaginate);

module.exports = mongoose.model(models.Medicines, MedicinesSchema);
