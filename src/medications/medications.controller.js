const mongoose = require('mongoose');

const { cloudinary } = require('../utils/cloudinaryClient');
const { models, PAGE_SIZE } = require('../utils/constants');
const logger = require('../utils/logger');

const Medicines = mongoose.model(models.Medicines);

module.exports = {
  async createMedicine(req, res) {
    try {
      const { image } = req.files;
      const ext = image.mimetype.split('/')[1];
      if (!['png', 'jpg', 'jpeg'].includes(ext.toLowerCase())) {
        return res.status(422).json({
          status: 422,
          message: "not a valid image"
        });
      }
      const { secure_url } = await cloudinary.uploader.upload(
        image.tempFilePath,
        {
          resource_type: 'raw',
          format: ext,
          folder: 'musala_medicines',
        }
      );

      const newMedicines = new Medicines(req.body);
      newMedicines.image = secure_url;
      const data = await newMedicines.save();
      return res.status(201).json({ data });
    } catch (error) {
      logger.error(error);
      res.status(500).send('Server Error');
    }
  },

  async getAllMedicine(req, res) {
    try {
      const page = Number(req.query.page || 1);
      const skip = (page - 1) * PAGE_SIZE;
      const total = await Medicines.find().countDocuments();
      const pages = Math.ceil(total / PAGE_SIZE);

      const filters = {};
      if (req.query.state) {
        filters.state = req.query.state
      }

      if (page > pages && page > 1) {
        return res.status(404).end('page not found');
      }

      const data = await Medicines.find(filters).sort({ createdAt: -1 }).skip(skip);
      return res.json({
        data,
        pages,
        total,
        page
      });
    } catch (error) {
      logger.error(error);
      res.status(500).send('Server Error');
    }
  },
}