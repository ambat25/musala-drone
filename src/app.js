const express = require('express');
const logger = require('./utils/logger');

const app = express();

const PORT = process.env.PORT || 8080;

require('./models');

require('./middlewares/startup')(app);

app.use('/api/v1/', require('./routes'));

app.listen(PORT, () => logger.info(`running on port: ${PORT}`));
