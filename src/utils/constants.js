const droneModels = ['Lightweight', 'Middleweight', 'Cruiserweight', 'Heavyweight'];

const deliveryStates = {
  INPROGRESS: 'INPROGRESS',
  COMPLETED: 'COMPLETED'
}

const droneStates = {
  IDLE: 'IDLE',
  LOADING: 'LOADING',
  LOADED: 'LOADED',
  DELIVERING: 'DELIVERING',
  DELIVERED: 'DELIVERED',
  RETURNING: 'RETURNING'
}

const models = {
  Drones: 'drones',
  Medicines: 'medicines',
  Delivery: 'delivery',
  DeliveryItems: 'DeliveryItems',
};


module.exports = {
  deliveryStates,
  droneModels,
  droneStates,
  models,

  PAGE_SIZE: 10,
};