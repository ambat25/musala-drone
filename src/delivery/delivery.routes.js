const express = require('express');
const { getAllDeliveries, getDeliveryById, createDelivery, loaded, returned, delivering, delivered, returning } = require('./delivery.controller');
const { deliveryById, validateSave } = require('./delivery.validator');

const router = express.Router();

router.get('/', getAllDeliveries);
router.get('/:id', deliveryById, getDeliveryById);
router.post('/:id/loaded', deliveryById, loaded);
router.post('/:id/delivering', deliveryById, delivering);
router.post('/:id/delivered', deliveryById, delivered);
router.post('/:id/returning', deliveryById, returning);
router.post('/:id/returned', deliveryById, returned);
router.post('/', validateSave, createDelivery);


module.exports = router;