const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const { deliveryStates, models } = require('../utils/constants');

const DeliverySchema = mongoose.Schema(
	{
		drone: {
			type: mongoose.Schema.Types.ObjectId,
			ref: models.Drones
		},
		medications: [{ type: mongoose.Schema.Types.ObjectId, ref: models.DeliveryItems }],
    state: {
			enum: Object.values(deliveryStates),
			required: true,
			type: String,
		},
		destination: {
			required: false,
			type: String,
		},
	},
	{
		timestamps: true,
	}
);

DeliverySchema.plugin(mongoosePaginate);

module.exports = mongoose.model(models.Delivery, DeliverySchema);
