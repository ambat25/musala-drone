const mongoose = require('mongoose');
const Joi = require('joi');
const { models, droneStates } = require('../utils/constants');

const Drones = mongoose.model(models.Drones);
const Delivery = mongoose.model(models.Delivery);
const Medicines = mongoose.model(models.Medicines);

module.exports = {
  async deliveryById(req, res, next){
    const schema = Joi.object({
      id: Joi.string(),
    });
  
    const { error } = schema.validate({
      id: req.params.id
    });
  
    if (error) {
      res.status(400).end({
        status: 400,
        message: error.details[0].message
      });
      return;
    }

    const delivery = await Delivery
      .findOne({ _id: req.params.id })
      .populate({
        path: 'medications',
        populate: { 
          path: 'item',
        }
      })
      .populate({
        path: 'drone'
      })

    if (!delivery?._id) {
      return res.status(404).json({
        status: 404,
        message: "Delivery does not exist",
      });
    }
    req.deliveryData = delivery;
    next();
  },

  async validateSave(req, res, next){
    let medicationItem = Joi.object().keys({
      item: Joi.string().required(),
      destination: Joi.string(),
      quantity: Joi.number().required(),
    })
    const schema = Joi.object({
      drone: Joi.string().required(),
      medications: Joi.array().items(medicationItem).required()
    });

    const { error } = schema.validate(req.body);
    if (error) {
      res.status(400).end(error.details[0].message);
      return;
    }

    const drone = await Drones.findOne({ _id: req.body.drone });

    if (!drone?._id) {
      return res.status(404).json({
        status: 404,
        message: "Drone does not exist",
      });
    }
    if (drone.state !== droneStates.IDLE) {
      return res.status(422).json({
        status: 422,
        message: "Drone is not idle",
      });
    }

    if (drone.batteryCapacity < 25) {
      return res.status(422).json({
        status: 422,
        message: "Drone battery is low",
      });
    }

    const medicines = await Medicines.find({
      '_id': { $in: (req.body.medications || [])?.map(med => mongoose.Types.ObjectId(med.item))}
    })

    if (medicines?.length !== req.body.medications?.length) {
      return res.status(422).json({
        status: 422,
        message: "One or more Medicine is not valid",
      });
    }
    const indexedMedicineQty = {};
    req.body.medications.forEach(item => {
      indexedMedicineQty[item.item] = item.quantity;
    })
    const totalWeight = medicines.reduce((acc, cur) => {
      return acc + (cur.weight * indexedMedicineQty[cur._id])
    }, 0)
    
    if (totalWeight > drone.weight){
      return res.status(422).json({
        status: 422,
        message: "Items are heavy",
      });
    }

    req.droneData = drone;
    req.medicinesData = medicines.map(d => ({
      item: d,
      quantity: indexedMedicineQty[d._id]
    }));
    next();
  }
}