const mongoose = require('mongoose');

const { models, PAGE_SIZE, deliveryStates, droneStates } = require('../utils/constants');
const logger = require('../utils/logger');

const Delivery = mongoose.model(models.Delivery);
const DeliveryItems = mongoose.model(models.DeliveryItems);

module.exports = {
  async createDelivery(req, res) {
    try {
      const medications = await DeliveryItems.insertMany(req.medicinesData)
      const drone = req.droneData;
      const newDelivery = new Delivery({
        drone: req.droneData,
        medications,
      });
      newDelivery.state = deliveryStates.INPROGRESS;

      drone.state = droneStates.LOADING;
      await drone.save();
      const data = await newDelivery.save();
      return res.status(201).json({ data });
    } catch (error) {
      logger.error(error);
      res.status(500).send('Server Error');
    }
  },

  async getAllDeliveries(req, res) {
    try {
      const page = Number(req.query.page || 1);
      const skip = (page - 1) * PAGE_SIZE;
      const total = await Delivery.find().countDocuments();
      const pages = Math.ceil(total / PAGE_SIZE);

      const filters = {};
      if (req.query.state) {
        filters.state = req.query.state
      }

      if (page > pages && page > 1) {
        return res.status(404).end('page not found');
      }

      const data = await Delivery.find(filters).sort({ createdAt: -1 }).skip(skip);
      return res.json({
        data,
        pages,
        total,
        page
      });
    } catch (error) {
      logger.error(error);
      return res.status(500).send('Server Error');
    }
  },

  async getDeliveryById(req, res) {
    try {
      return res.status(200).json({ data: req.deliveryData });
    } catch (error) {
      logger.error(error);
      return res.status(500).send('Server Error');
    }
  },

  async loaded(req, res){
    const drone = req.deliveryData?.drone;

    if (drone?.state !== droneStates.LOADING) {
      return res.status(422).json({
        status: 422,
        message: "Drone cannot be in loaded if it wasn't in loading",
      });
    }

    if (drone?.state !== droneStates.LOADED) {
      return res.status(422).json({
        status: 422,
        message: "Drone is already loaded",
      });
    }
    drone.state = droneStates.LOADED;
    await drone.save();
    return res.status(200).json({ data: drone });
  },

  async delivering(req, res){
    const drone = req.deliveryData?.drone;

    if (drone?.state !== droneStates.LOADED) {
      return res.status(422).json({
        status: 422,
        message: "Drone cannot be in delivering if it wasn't loaded",
      });
    }

    if (drone?.state === droneStates.DELIVERING) {
      return res.status(422).json({
        status: 422,
        message: "Drone is already delivering",
      });
    }
    drone.state = droneStates.DELIVERING;
    await drone.save();
    return res.status(200).json({ data: drone });
  },

  async delivered(req, res){
    const drone = req.deliveryData?.drone;

    if (drone?.state !== droneStates.DELIVERING) {
      return res.status(422).json({
        status: 422,
        message: "Drone cannot be in delivered if it wasn't delivering",
      });
    }

    if (drone?.state === droneStates.DELIVERED) {
      return res.status(422).json({
        status: 422,
        message: "Drone is already delivered",
      });
    }
    drone.state = droneStates.DELIVERED;
    await drone.save();
    return res.status(201).json({ data: drone });

  },

  async returning(req, res){
    const drone = req.deliveryData?.drone;

    if (drone?.state !== droneStates.DELIVERED) {
      return res.status(422).json({
        status: 422,
        message: "Drone cannot be in return if it wasn't delivered",
      });
    }

    if (drone?.state === droneStates.RETURNING) {
      return res.status(422).json({
        status: 422,
        message: "Drone is already returning",
      });
    }
    drone.state = droneStates.RETURNING;
    await drone.save();
    return res.status(201).json({ data: drone });
  },

  async returned(req, res){
    const drone = req.deliveryData?.drone;
    const delivery = req.deliveryData;

    if (drone?.state !== droneStates.RETURNING) {
      return res.status(422).json({
        status: 422,
        message: "Drone cannot be in returned if it wasn't returning",
      });
    }

    if (drone?.state === droneStates.IDLE) {
      return res.status(422).json({
        status: 422,
        message: "Drone is already returened",
      });
    }
    drone.state = droneStates.IDLE;
    delivery.state = deliveryStates.COMPLETED;
    await drone.save();
    return res.status(201).json({ data: drone });

  },



}

