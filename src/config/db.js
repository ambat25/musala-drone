module.exports = {
  url: process.env.DB_URL || 'mongodb://mongo',
  name: process.env.DB_NAME || 'drone',
};