const mongoose = require('mongoose');

const { models, PAGE_SIZE, droneStates } = require('../utils/constants');
const logger = require('../utils/logger');

const Drones = mongoose.model(models.Drones);

module.exports = {
  async createDrone(req, res) {
    try {
      const newDrones = new Drones(req.body);
      const data = await newDrones.save();
      return res.status(201).json({ data });
    } catch (error) {
      logger.error(error);
      res.status(500).send('Server Error');
    }
  },

  async getAllDrones(req, res) {
    try {
      const page = Number(req.query.page || 1);
      const skip = (page - 1) * PAGE_SIZE;
      const total = await Drones.find().countDocuments();
      const pages = Math.ceil(total / PAGE_SIZE);

      const filters = {};
      if (req.query.state) {
        filters.state = req.query.state
      }

      if (page > pages && page > 1) {
        return res.status(404).end('page not found');
      }

      const drones = await Drones.find(filters).sort({ createdAt: -1 }).skip(skip);
      return res.json({
        data: drones,
        pages,
        total,
        page
      });
    } catch (error) {
      logger.error(error);
      return res.status(500).send('Server Error');
    }
  },

  async getDroneBySerialNumber(req, res) {
    try {
      return res.json({ data: req.droneData });
    } catch (error) {
      logger.error(error);
      return res.status(500).send('Server Error');
    }
  },

  async getDroneBatteryCapacityBySerialNumber(req, res) {
    try {
      return res.json({ data: req.droneData.batteryCapacity });
    } catch (error) {
      logger.error(error);
      return res.status(500).send('Server Error');
    }
  },

  async updateDroneBattery(req, res) {
    try {
      const drone = req.droneData;
      drone.batteryCapacity = req.body.batteryCapacity;
      await drone.save();
      return res.json({ data: drone });
    } catch (error) {
      logger.error(error);
      return res.status(500).send('Server Error');
    }
  },
}

