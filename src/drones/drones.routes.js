const express = require('express');

const { getAllDrones, createDrone, getDroneBySerialNumber, updateDroneBattery, getDroneBatteryCapacityBySerialNumber } = require('./drones.controller');
const { validateSave, validateDroneBySerialNumber, validateFetchAll, validateDroneBattery } = require('./drones.validator');

const router = express.Router();

router.get('/', validateFetchAll, getAllDrones);
router.get('/:serialNumber', validateDroneBySerialNumber, getDroneBySerialNumber);
router.get('/:serialNumber/battery', validateDroneBySerialNumber, getDroneBatteryCapacityBySerialNumber);
router.put('/:serialNumber/battery', validateDroneBattery, validateDroneBySerialNumber, updateDroneBattery);
router.post('/', validateSave, createDrone);


module.exports = router;