const Joi = require('joi');
const mongoose = require('mongoose');

const { droneModels, models, droneStates } = require('../utils/constants');
const { formatValidationErrorMessages } = require('../utils/helpers');

const Drones = mongoose.model(models.Drones);

module.exports = {

  async validateSave(req, res, next) {
    const schema = Joi.object({
      serialNumber: Joi.string().max(100).required(),
      model: Joi.string().valid(...droneModels).required(),
      weight: Joi.number().greater(0).max(500).required(),
      state: Joi.string().required(),
      batteryCapacity: Joi.number().min(0).max(100).required(),
    });

    const droneDuplicate = await Drones.findOne({ serialNumber: req.body.serialNumber });

    if (droneDuplicate && droneDuplicate._id) {
      return res.status(400).json({
        status: 400,
        message: `Drone with serial number: "${droneDuplicate.serialNumber}" already exists`,
      });
    }

    const { error } = schema.validate(req.body, { abortEarly: false });

    if (error) {
      const errorMessages = formatValidationErrorMessages(error);
      return res.status(422).json({
        status: 422,
        message: "failed validation",
        errorMessages
      });
    }
    next();
  },

  async validateDroneBySerialNumber(req, res, next) {
    try {
      const data = await Drones.findOne({ serialNumber: req.params.serialNumber });

      if (data?._id) {
        req.droneData = data
        return next();
      }
      return res.status(404).json({
        status: 404,
        message: "Drone does not exist",
      });
    } catch (error) {
      return res.status(422).json({
        status: 422,
        message: "failed validation",
      });
    }
  },

  async validateDroneBattery(req, res, next) {
    try {

      const schema = Joi.object({
        batteryCapacity: Joi.number().min(0).max(100).required(),
      });

      const { error } = schema.validate(req.body, { abortEarly: false });

      if (error) {
        const errorMessages = formatValidationErrorMessages(error);
        return res.status(422).json({
          status: 422,
          message: "failed validation",
          errorMessages
        });
      }
      next()
    } catch (error) {
      return res.status(422).json({
        status: 422,
        message: "failed validation",
      });
    }
  },

  validateFetchAll(req, res, next) {

    if (req.query.state && !droneStates[req.query.state]) {
      return res.status(422).json({
        status: 422,
        message: 'Invalid state provided',
      });
    }
    next()
  }
};
