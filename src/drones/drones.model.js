const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const { droneModels, droneStates, models } = require('../utils/constants');

const DronesSchema = mongoose.Schema(
	{
		serialNumber: {
      maxLength: 100,
			required: true,
			type: String,
			unique: true,
		},
		model: {
      enum: droneModels,
			required: true,
			type: String,
		},
		weight: {
			required: true,
			type: Number,
      max: 500,
		},
		state: {
			enum: Object.values(droneStates),
			required: true,
			type: String,
		},
		batteryCapacity: {
			type: Number,
			required: true,
      max: 100,
      min: 0,
		}
	},
	{
		timestamps: true,
	}
);

DronesSchema.plugin(mongoosePaginate);

module.exports = mongoose.model(models.Drones, DronesSchema);
