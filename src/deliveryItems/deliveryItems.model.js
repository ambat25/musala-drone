const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const { models } = require('../utils/constants');

const { Schema } = mongoose;

const DeliveryItemsSchema = new Schema({
  item: {
    type: Schema.Types.ObjectId,
    ref: models.Medicines,
    required: true
  },
  quantity: {
    type: Number,
    required: true
  },
});

DeliveryItemsSchema.plugin(mongoosePaginate);

mongoose.model(models.DeliveryItems, DeliveryItemsSchema);