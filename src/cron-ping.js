const mongoose = require('mongoose');
const cron = require('node-cron');
const { createLogger, transports, format } = require('winston');

const logger = createLogger({
  format: format.combine(
    format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss:ms' }),
    format.printf((info) => `${info.timestamp} ${info.level}: ${info.message}`),
  ),
  transports: [
    new transports.File({
      filename: './logs/drones-battery-logs.log',
      json: false,
      maxsize: 5242880,
      maxFiles: 5,
    })
  ],
});

require('./models');
const { models } = require('./utils/constants');
const Drones = mongoose.model(models.Drones);

cron.schedule('* * * * *', async () =>  {
  const drones = await Drones.find().select('serialNumber, batteryCapacity')
  logger.info(new Date().toISOString());
  logger.info(drones);
  logger.info('');
});
