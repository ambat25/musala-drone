const express = require('express');

const router = express.Router();

router.use('/drones', require('./drones/drones.routes'));
router.use('/medications', require('./medications/medications.routes'));
router.use('/deliveries', require('./delivery/delivery.routes'));

module.exports = router;