const express = require('express');

const morganLogger = require('morgan');
const errorhandler = require('errorhandler');
const fileUpload = require('express-fileupload');
const cors = require('cors');
const notifier = require('node-notifier');

module.exports = (app) => {
  app.use(cors());
  app.use(morganLogger('dev'));
  app.use(fileUpload({ useTempFiles: true }));
  app.use(express.json());
  app.use(express.urlencoded({ extended: false }));
  if (process.env.NODE_ENV !== 'production') {
    const errorNotification = (err, str, req) => {
      const title = `Error in ${req.method} ${req.url}`;

      notifier.notify({
        title,
        message: str,
      });
    };
    app.use(errorhandler({ log: errorNotification }));
  }
};