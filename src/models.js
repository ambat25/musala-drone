const mongoose = require('mongoose');
const logger = require('./utils/logger');
const db = require('./config/db');

// Configure mongoose's promise to global promise
mongoose.promise = global.Promise;

// Configure Mongoose
const mongodbURL = `${db.url}/${db.name}?retryWrites=true&w=majority`;
mongoose
  .connect(mongodbURL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    logger.info('db connected successfully');
  })
  .catch((error) => {
    logger.error(error.message);
  });
mongoose.set('debug', true);

// setup models
require('./drones/drones.model');
require('./medications/medications.model');
require('./delivery/delivery.model');
require('./deliveryItems/deliveryItems.model');
