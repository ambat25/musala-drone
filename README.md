# Musala Drone
Musala Drone


## System Requirements

- [Node 12.x LTS](https://nodejs.org/en/blog/release/v12.16.3/)
- [Yarn 1.x](https://yarnpkg.com/)

### Local Development Setup

1. Clone the repository

```bash
git clone git@gitlab.com:ambat25/musala-drone.git
```

2. Install all dependencies

```bash
yarn install --no-lockfile --production=false --silent
```

3. Create env file and fill it with appropriate data
```bash
cp sample.env .env
```

4. Update content fo .env

4. Start the server
```bash
yarn start
```


### API Endpoints
1. Get All Medications
```
curl --request GET \
  --url http://localhost:8080/api/v1/medications
  ```

2. Create Medication
```
curl --request POST \
  --url http://localhost:8080/api/v1/medications \
  --header 'Content-Type: multipart/form-data; boundary=---011000010111000001101001' \
  --form name=123red \
  --form code=AADD12 \
  --form 'image=@/Users/ambat/Downloads/im(1).png' \
  --form weight=50
```

3. Create Drone
```
curl --request POST \
  --url http://localhost:8080/api/v1/drones \
  --header 'Content-Type: application/json' \
  --data '{
	"serialNumber": "122k3a5",
	"model": "Lightweight",
	"weight": 500,
	"state": "IDLE",
	"batteryCapacity": 12
}
```

4. Fetch Drones
```
curl --request GET \
  --url http://localhost:8080/api/v1/drones
```

5. Fetch Drone by serial number
```
curl --request GET \
  --url http://localhost:8080/api/v1/drones/:droneId
```

6. Update Drone Battery capacity
```
curl --request GET \
  --url http://localhost:8080/api/v1/drones/:droneId \
  --header 'Content-Type: application/json' \
  --data '{
	"batteryCapacity": 50
}'
```

7. Get Drone Battery Capacity
```
curl --request GET \
  --url http://localhost:8080/api/v1/drones/:droneId/battery
```

8. Get All Deliveries
```
curl --request GET \
  --url http://localhost:8080/api/v1/deliveries
```

9. Get Delivery By Id
```
curl --request POST \
  --url http://localhost:8080/api/v1/deliveries/61fa442f64a8c0e99d2f48b4
```

10. Create Delivery
```
curl --request POST \
  --url http://localhost:8080/api/v1/deliveries \
  --header 'Content-Type: application/json' \
  --data '{
	"drone": "61fa441e64a8c0e99d2f48ac",
	"medications": [
		{ "item": "61fa2ce7183fe362bc579865", "quantity": 2 }
	]
}
```

11. Loaded Drone Delivery
```
curl --request PUT \
  --url http://localhost:8080/api/v1/deliveries/:deliveryId/loaded
```


12. Delivering Drone Delivery
```
curl --request PUT \
  --url http://localhost:8080/api/v1/deliveries/:deliveryId/delivering
```

12. Delivered Drone Delivery
```
curl --request PUT \
  --url http://localhost:8080/api/v1/deliveries/:deliveryId/delivered
```

12. Returning Drone Delivery
```
curl --request PUT \
  --url http://localhost:8080/api/v1/deliveries/:deliveryId/returning
```

12. Returned Drone Delivery
```
curl --request PUT \
  --url http://localhost:8080/api/v1/deliveries/:deliveryId/returned
```
